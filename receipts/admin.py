from django.contrib import admin
from .models import ExpenseCategory, Receipt, Account


admin.site.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'owner',
    )


admin.site.register(Receipt)
list_display = (
        'vendor',
        'total',
        'tax',
        'date',
        'purchaser',
        'category',
        'account',
    )


admin.site.register(Account)
list_display = (
        'name',
        'number',
        'owner',
    )
